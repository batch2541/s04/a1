package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    @RequestMapping(value="/posts", method= RequestMethod.POST)
    //"ResponseEntity" represents the whole HTTP response: status code, headers, and body.


    public ResponseEntity<Object> createPost(@RequestHeader(value="Authorization")
                                             String stringToken, @RequestBody Post post) {

        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }


    @RequestMapping(value="/posts", method= RequestMethod.GET)

    public ResponseEntity<Object> getPosts() {

        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

} 

